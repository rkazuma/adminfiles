create table M_ADM_OFFICE(
    id varchar2(10) primary key,
    name varchar2(200) not null,
    status number(1) not null
);

create table M_ADM_fingerprint_point(
    id varchar(10) primary key,
    name varchar2(200) not null,
    ip varchar2(30) not null,
    mac varchar2(30) not null,
    office_id varchar2(10) not null,
    username varchar2(30) not null,
    status number(1) not null
);

create table M_ADM_DEVICE(
    id number(10) primary key,
    name varchar2(200) not null,
    sn varchar2(30),
    fpoint_id varchar(10),
    status number(1) not null
);

ALTER TABLE M_ADM_DEVICE ADD CONSTRAINT fk_device_fpoint
    FOREIGN KEY (fpoint_id)
    REFERENCES M_ADM_fingerprint_point(id);

ALTER TABLE M_ADM_fingerprint_point ADD CONSTRAINT fk_fpoint_office
    FOREIGN KEY (office_id)
    REFERENCES M_ADM_OFFICE(id);

--logs
create table M_ADM_LOG_fingerprint_point(
    id number(16) primary key,
    username varchar2(30) not null,
    created DATE not null,
    data varchar2(2000) not null
);
CREATE TABLE M_ADM_CONFIGURATION(
name varchar2(50) primary key,
value varchar2(100) not null
);

Insert into M_ADM_CONFIGURATION (NAME,VALUE) values ('validate_point_device','S');
Insert into M_ADM_CONFIGURATION (NAME,VALUE) values ('validate_device','S');
Insert into M_ADM_CONFIGURATION (NAME,VALUE) values ('validate_point','S');

