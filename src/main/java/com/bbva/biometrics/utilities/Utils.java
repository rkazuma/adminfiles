package com.bbva.biometrics.utilities;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Utils {

	@Value("${log.ruta}")
	private String rutaLog;
	private static final Logger LOGGER = Logger.getLogger(Utils.class.getName());
	private Constantes cons;

	public void initLog() {
		FileHandler fh;
		try {
			String rutaname = rutaLog + cons.LOG_NAME + this.getFechaStringFormat("yyyyMMdd") + ".txt";
			fh = new FileHandler(rutaname);
			LOGGER.addHandler(fh);
			SimpleFormatter formatter = new SimpleFormatter();
			fh.setFormatter(formatter);
		} catch (SecurityException | IOException e) {

		}
	}

	public void logInfo(String mensaje, String id) {
		this.initLog();
		String msg = id + " : " + mensaje;
		LOGGER.log(Level.INFO, msg);
	}

	public void logError(String mensaje, String id, Exception e) {
		this.initLog();
		String msg = id + " : " + mensaje;
		LOGGER.log(Level.SEVERE, msg, e);
	}

	public void logWarn(String mensaje, String id) {
		this.initLog();
		String msg = id + " : " + mensaje;
		LOGGER.log(Level.WARNING, msg);
	}

	public String getFechaString() {
		String fecha = null;
		Date fechaActual = new Date();
		DateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		fecha = formatoFecha.format(fechaActual);
		return fecha;
	}

	public String getFechaFullString() {
		String fecha = null;
		Date fechaActual = new Date();
		DateFormat formatoFecha = new SimpleDateFormat("yyyyMMddHHmm");
		fecha = formatoFecha.format(fechaActual);
		return fecha;
	}

	public String getFechaStringFormat(String format) {
		String fecha = null;
		Date fechaActual = new Date();
		DateFormat formatoFecha = new SimpleDateFormat(format);
		fecha = formatoFecha.format(fechaActual);
		return fecha;
	}
}
