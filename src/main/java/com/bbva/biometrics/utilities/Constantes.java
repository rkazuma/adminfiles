package com.bbva.biometrics.utilities;

public class Constantes {

	public static final String LOG_NAME = "admfiles_";
	public static final String CMD_LS = "ls ";
	public static final String CMD_LS_DIR = "ls -d ";
	public static final String CMD_LS_DIR2 = "ls -d ./*/ ";
	public static final String CMD_DIR_REMOTE = "ls ./home/CE55866/nfs";
	public static final String CMD_DIR_REMOTE2 = "ls -d ./home/CE55866/nfs/*/";
	public static final String CMD_CD = "cd ";
	public static final String CMD_TEMP_W = "C:\\temp\\";
	public static final String CMD_TEMP_U = "/tmp";
	public static final String CMD_FOLDER = "/home/CE55866/nfs";
	public static final String CMD_COPY = "scp ";
	public static final String CMD_COPY_W = "pscp.exe ";
	public static final String CMD_FIND_NAME1 = "grep -iRl \"";
	public static final String CMD_FIND_NAME2 = "\" ./";

	public static final String LS_REMOTE_INIT = "ls -d ";
	public static final String LS_REMOTE_END = "/*/*";
	public static final String LS_REMOTED_END = "/date/*";
	public static final String FIND_INIT = "find ";
	public static final String FIND_END = " -name \"";
	public static final String FIND_END_E = "*\"";
	public static final String C_DATE = "date";

}
