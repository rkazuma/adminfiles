package com.bbva.biometrics.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.stereotype.Repository;

import com.bbva.biometrics.model.Usuario;
 
 
@Repository("userDao")
public class UserRepositorympl extends AbstractDao<String, Usuario> implements UserRepository {
	@Override
    public Usuario findByToken(String id) {
    	Usuario user = null;
    	try {
    		user = (Usuario) getEntityManager().createNamedQuery("Usuario.findByTicket").setParameter(1, id).setMaxResults(1).getSingleResult();
    	}catch (NoResultException e) {
			e.printStackTrace();
		}
        return user;
    }

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> findAll() {
		List<Usuario> users  = new ArrayList <Usuario>();
    	try {    		
    		users = getEntityManager().createNamedQuery("Usuario.findAll").getResultList();    		
    	}catch (NoResultException e) {
			e.printStackTrace();
		}
        return users;    
	}

	@Override
	public void saveAndFlush(Usuario of) {
		try {
			update(of);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
