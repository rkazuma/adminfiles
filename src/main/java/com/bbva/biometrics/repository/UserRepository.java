package com.bbva.biometrics.repository;

import java.util.List;

import com.bbva.biometrics.model.Usuario;

public interface UserRepository {
	List<Usuario> findAll();
	Usuario findByToken(String id);
	void saveAndFlush(Usuario of);
}
