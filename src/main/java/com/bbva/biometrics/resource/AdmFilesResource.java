package com.bbva.biometrics.resource;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.biometrics.dto.FileDocument;
import com.bbva.biometrics.dto.UserFileDocument;
import com.bbva.biometrics.service.IAdmFilesService;

@RestController
@RequestMapping(value = "/adm-files")
//@CrossOrigin(origins = "*", methods = { RequestMethod.GET, RequestMethod.POST })
public class AdmFilesResource {

	@Autowired
	private IAdmFilesService admFilesService;

	@Transactional(readOnly = true)
	@RequestMapping(value = "/health")
	public Boolean healt() {
		return true;
	}

	@Transactional(readOnly = true)
	@RequestMapping(value = "/healthconnection")
	public ResponseEntity<Object> healtConnection() {
		Object response = admFilesService.testConnection();
		if (response != null) {
			return new ResponseEntity<Object>(response, HttpStatus.OK);
		}
		return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/getlist")
	public ResponseEntity<Object> getList() {
		Object response = admFilesService.getList();
		if (response != null) {
			return new ResponseEntity<Object>(response, HttpStatus.OK);
		}
		return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/getlistbydate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getListByDir(@PathParam("date") String date) {
		System.out.println("Date: " + date);
		Object response = admFilesService.findListByDate(date);
		if (response != null) {
			return new ResponseEntity<Object>(response, HttpStatus.OK);
		}
		return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/getfilebyname", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getFileByDir(@PathParam("id") String id) {
		Object response = admFilesService.findListByName(id);
		if (response != null) {
			return new ResponseEntity<Object>(response, HttpStatus.OK);
		}
		return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = "/getfile", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getFile(@PathParam("id") String id, @PathParam("date") String date) {
		System.out.println("PathParam: " + date + "/" + id);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setContentDispositionFormData(id, id);

		byte[] response = admFilesService.findFile(date + "/" + id);
		if (response != null) {
			return new ResponseEntity<byte[]>(response, headers, HttpStatus.OK);
		}
		return new ResponseEntity<byte[]>(response, HttpStatus.NOT_FOUND);
	}

}
