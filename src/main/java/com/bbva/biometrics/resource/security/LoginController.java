package com.bbva.biometrics.resource.security;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.biometrics.dto.UserDTO;
import com.bbva.biometrics.dto.security.LoginRequest;
import com.bbva.biometrics.service.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.TextCodec;

@RestController
@PropertySource(value = { "classpath:application.properties" })
public class LoginController {
 
  @Value("${jwt.secret}")
  @NotNull
  private String secret;
  
  @Autowired
  private UserService userService;
 
  @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<HashMap<String, String>> login(@RequestBody @Valid final LoginRequest login) throws ServletException {
	 //validate user
    final UserDTO user = userService.getUser(login.getTicket());
    if (user == null) {
      return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
    }
    
    final Instant now = Instant.now();
    // System.out.println("token : " + secret);
    final String jwt = Jwts.builder()
        .setSubject(user.getLoginUsuario())
        .setIssuedAt(Date.from(now))
        .setExpiration(Date.from(now.plus(1, ChronoUnit.DAYS)))
        .signWith(SignatureAlgorithm.HS256, TextCodec.BASE64.encode(secret))
        .compact();
    HashMap<String, String> res = new HashMap<>();
    res.put("token", jwt);
    return new ResponseEntity<>(res, HttpStatus.OK);
  }
 
}