package com.bbva.biometrics.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.bbva.biometrics.dto.UserDTO;
import com.bbva.biometrics.service.UserService;

@RestController
@RequestMapping(value = "/api/user")
public class UserResource {
	
	@Autowired
	private UserService userService;
	
	@Transactional(readOnly = true)
	@RequestMapping(method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<UserDTO>> getAll() {
		List<UserDTO> res = userService.getListOffice();
		return new ResponseEntity<List<UserDTO>>(res, HttpStatus.OK);
	}
}
