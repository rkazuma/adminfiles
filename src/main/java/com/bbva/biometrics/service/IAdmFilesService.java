package com.bbva.biometrics.service;

public interface IAdmFilesService {

	Object testConnection();

	Object getList();

	Object getListByDate(String dir);

	byte[] getFile(String dirname);

	Object getListByName(String name);

	// NEW

	Object findList();

	Object findListByName(String ident);

	Object findListByDate(String date);

	byte[] findFile(String dirname);

}
