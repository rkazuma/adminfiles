package com.bbva.biometrics.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.biometrics.dto.FileDocument;
import com.bbva.biometrics.dto.UserFileDocument;
import com.bbva.biometrics.ssh.SSHConnector;
import com.bbva.biometrics.utilities.Constantes;
import com.bbva.biometrics.utilities.Utils;
import com.jcraft.jsch.JSchException;

@Service
@Transactional
public class AdmFilesService implements IAdmFilesService {

	@Autowired
	private Utils util;
	@Autowired
	private SSHConnector ssh;
	@Value("${ssh.user}" + "@" + "${ssh.ip_dir}")
	private String usernamehost;
	@Value("${ssh.pathRemote}")
	private String pathRemote;
	@Value("${ssh.pathLocal}")
	private String pathLocal;
	private Constantes cons;

	@Override
	public Object testConnection() {
		Object response = false;
		try {
			if (ssh.connect()) {
				response = true;
				ssh.disconnect();
				return response;
			}
		} catch (IllegalAccessException | JSchException e) {
			util.logError("[testConnection] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	@Override
	public Object getList() {
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		try {
			if (ssh.connect()) {
				String result = ssh.executeCommand(cons.CMD_DIR_REMOTE2);
				String arr[] = result.split("\n");
				for (String s : arr) {
					System.out.println("> " + s);
					FileDocument doc = new FileDocument();
					doc.setUrlfile(s);
					documents.add(doc);
				}
				userDoc.setFiles(documents);
				return documents;
			}
		} catch (Exception e) {
			util.logError("[getList] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	@Override
	public Object getListByDate(String date) {
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		try {
			if (ssh.connect()) {
				String result = ssh.executeCommand(cons.CMD_DIR_REMOTE + "/" + date);
				String arr[] = result.split("\n");
				for (String s : arr) {
					System.out.println("> " + s);
					FileDocument doc = new FileDocument();
					doc.setUrlfile("/" + date + "/" + s);
					documents.add(doc);
				}
				userDoc.setFiles(documents);
				return userDoc;
			}
		} catch (Exception e) {
			util.logError("[getListByDir] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	@Override
	public Object getListByName(String name) {
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		try {
			if (ssh.connect()) {
				String result = ssh.executeCommand(cons.CMD_FIND_NAME1 + name + cons.CMD_FIND_NAME2);
				String arr[] = result.split("\n");
				if (arr.length == 0) {
					for (String s : arr) {
						System.out.println("> " + s);
						FileDocument doc = new FileDocument();
						String ar[] = s.split("/");
						doc.setUrlfile("/" + ar[2] + "/" + ar[3]);
						documents.add(doc);
					}
					userDoc.setFiles(documents);
				}
				return userDoc;
			}
		} catch (Exception e) {
			util.logError("[getListByDir] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	@Override
	public byte[] getFile(String dirname) {
		byte[] response = null;
		try {
			if (true) {// <ssh.connect()) {
				// scp username@remotehost:foobar.txt /some/local/directory
				// pscp.exe username@x.x.x.x:/file_path/filename c:\directory\filename
				String command = cons.CMD_COPY + usernamehost + ":" + cons.CMD_FOLDER + dirname + " " + cons.CMD_TEMP_U;
				String result = ssh.executeCommand(command);
				// String result =
				// "/cld_biometria_fiduciaria_nfs/2019-03-27/fidusap_201903141646.pdf";
				System.out.println(result);
				if (result != null) {
					String ar[] = dirname.split("/");
					// chante this to CMD_TEMP_U
					File file = new File(cons.CMD_TEMP_U + ar[2]);
					// File file = new File(cons.CMD_TEMP_W + "fidusap_201903141646.pdf");// ar[2]);
					response = readFileToByteArray(file);
				}
				return response;
			}
		} catch (Exception e) {
			util.logError("[getFile] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	private byte[] readFileToByteArray(File file) {
		FileInputStream fis = null;
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		FileDocument doc = new FileDocument();
		byte[] bArray = new byte[(int) file.length()];
		try {
			fis = new FileInputStream(file);
			fis.read(bArray);
			fis.close();
		} catch (IOException e) {
			util.logError("[readFileToByteArray] ", e.getMessage(), e);
		}
		// Setters
		doc.setUrlfile(file.getPath());
		doc.setFile(bArray);
		documents.add(doc);
		userDoc.setFiles(documents);
		// return userDoc // Get 1
		return bArray; // Get 2
	}

	public Object findList() {
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		try {
			if (ssh.connect()) {
				String command = cons.LS_REMOTE_INIT + pathRemote + cons.LS_REMOTE_END;
				String result = ssh.executeCommand(command);
				String arr[] = result.split("\n");
				for (String s : arr) {
					FileDocument doc = new FileDocument();
					String ar[] = s.split("/");
					doc.setName(ar[3]);
					doc.setUrlfile(s);
					documents.add(doc);
				}
				userDoc.setFiles(documents);
				return userDoc;
			}
		} catch (IllegalAccessException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} catch (JSchException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} catch (IOException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	public Object findListByName(String ident) {
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		try {
			if (ssh.connect()) {
				String command = cons.FIND_INIT + pathRemote + cons.FIND_END + ident + cons.FIND_END_E;
				String result = ssh.executeCommand(command);
				String arr[] = result.split("\n");
				if (arr.length > 0 && !arr[0].isEmpty()) {
					for (String s : arr) {
						FileDocument doc = new FileDocument();
						String ar[] = s.split("/");
						doc.setName(ar[3]);
						doc.setUrlfile(s);
						documents.add(doc);
					}
					userDoc.setFiles(documents);
				}
				return userDoc;
			}
		} catch (IllegalAccessException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} catch (JSchException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} catch (IOException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	public Object findListByDate(String date) {
		UserFileDocument userDoc = new UserFileDocument();
		List<FileDocument> documents = new ArrayList<FileDocument>();
		try {
			if (ssh.connect()) {
				String command = cons.LS_REMOTE_INIT + pathRemote + cons.LS_REMOTED_END.replace(cons.C_DATE, date);
				String result = ssh.executeCommand(command);
				String arr[] = result.split("\n");
				if (arr.length > 0 && !arr[0].isEmpty()) {
					for (String s : arr) {
						FileDocument doc = new FileDocument();
						String ar[] = s.split("/");
						doc.setName(ar[3]);
						doc.setUrlfile(s);
						documents.add(doc);
					}
					userDoc.setFiles(documents);
				}
				return userDoc;
			}
		} catch (IllegalAccessException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} catch (JSchException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} catch (IOException e) {
			util.logError("[findList] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}

	public byte[] findFile(String dirname) {
		byte[] response = null;
		try {
			if (ssh.connect()) {
				if (ssh.getFile(pathRemote + "/" + dirname)) {
					String ar[] = dirname.split("/");
					String url = pathLocal + "/" + ar[1];
					File file = new File(url);
					response = readFileToByteArray(file);
				}
			}
			return response;
		} catch (Exception e) {
			util.logError("[getFile] ", e.getMessage(), e);
		} finally {
			ssh.disconnect();
		}
		return null;
	}
}
