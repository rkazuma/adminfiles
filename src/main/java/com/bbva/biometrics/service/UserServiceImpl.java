package com.bbva.biometrics.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.biometrics.dto.UserDTO;
import com.bbva.biometrics.model.Usuario;
import com.bbva.biometrics.repository.UserRepository;
 
 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService{
 
    @Autowired
    private UserRepository userRepository;
 
    public UserDTO getUser(String id) {
    	UserDTO user = null;
    	Usuario u = userRepository.findByToken(id);
    	if(u != null) {
    		user = new UserDTO();
			user.setLoginUsuario(u.getLoginUsuario());
			user.setFirmaUrl(u.getFirmaUrl());
			user.setDescripcionUsuario(u.getDescripcionUsuario());
			// remove token
			u.setFirmaUrl(null);
			userRepository.saveAndFlush(u);
    	}
        return user;
    }

	@Override
	public List<UserDTO> getListOffice() {
		final List<UserDTO> users = new ArrayList<>();
		List<Usuario> list = userRepository.findAll();
		if (list != null) {
			for(Usuario of: list) {
				users.add(new UserDTO(of.getLoginUsuario(), of.getDescripcionUsuario(), of.getEstadoUsuario(), of.getTipoUsuario()));
			}
		}
		return users;
	}
}
