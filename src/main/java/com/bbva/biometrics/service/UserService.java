package com.bbva.biometrics.service;

import java.util.List;

import com.bbva.biometrics.dto.UserDTO;

public interface UserService {
	List<UserDTO> getListOffice();
	UserDTO getUser(String id);
}
