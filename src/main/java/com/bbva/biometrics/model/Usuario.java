package com.bbva.biometrics.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the USUARIOS database table.
 * 
 */
@Entity
@Table(name="USUARIOS")
@NamedQueries({
	@NamedQuery(name="Usuario.findAll", query="SELECT u FROM Usuario u where u.estadoUsuario = 'A' order by u.loginUsuario"),
	@NamedQuery(name="Usuario.findByTicket", query="SELECT u FROM Usuario u where u.firmaUrl = ?")	
})

public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="LOGIN_USUARIO", unique=true, nullable=false, length=20)
	private String loginUsuario;

	@Column(name="CLASE_DOCUMENTO", length=3)
	private String claseDocumento;

	@Column(name="CODIGO_GRUPO_USUARIO", nullable=false, precision=5)
	private BigDecimal codigoGrupoUsuario;

	@Column(length=100)
	private String correo;

	@Column(name="DESCRIPCION_USUARIO", nullable=false, length=40)
	private String descripcionUsuario;

	@Column(name="ESTADO_USUARIO", nullable=false, length=1)
	private String estadoUsuario;

	@Column(name="FIRMA_URL", length=300)
	private String firmaUrl;

	private BigDecimal identificacion;

	@Column(length=20)
	private String ip;

	@Column(name="NOMBRE_MAQUINA", length=20)
	private String nombreMaquina;

	@Column(name="OFICINA_INGRESO_WEB", precision=5)
	private BigDecimal oficinaIngresoWeb;

	@Column(name="PERMISO_WEB", length=3)
	private String permisoWeb;

	@Column(precision=4)
	private BigDecimal tipo;

	@Column(name="TIPO_USUARIO", length=3)
	private String tipoUsuario;

	@Column(length=20)
	private String usuario;

	public Usuario() {
	}

	public String getLoginUsuario() {
		return this.loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public String getClaseDocumento() {
		return this.claseDocumento;
	}

	public void setClaseDocumento(String claseDocumento) {
		this.claseDocumento = claseDocumento;
	}

	public BigDecimal getCodigoGrupoUsuario() {
		return this.codigoGrupoUsuario;
	}

	public void setCodigoGrupoUsuario(BigDecimal codigoGrupoUsuario) {
		this.codigoGrupoUsuario = codigoGrupoUsuario;
	}

	public String getCorreo() {
		return this.correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getDescripcionUsuario() {
		return this.descripcionUsuario;
	}

	public void setDescripcionUsuario(String descripcionUsuario) {
		this.descripcionUsuario = descripcionUsuario;
	}

	public String getEstadoUsuario() {
		return this.estadoUsuario;
	}

	public void setEstadoUsuario(String estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}

	public String getFirmaUrl() {
		return this.firmaUrl;
	}

	public void setFirmaUrl(String firmaUrl) {
		this.firmaUrl = firmaUrl;
	}

	public BigDecimal getIdentificacion() {
		return this.identificacion;
	}

	public void setIdentificacion(BigDecimal identificacion) {
		this.identificacion = identificacion;
	}

	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getNombreMaquina() {
		return this.nombreMaquina;
	}

	public void setNombreMaquina(String nombreMaquina) {
		this.nombreMaquina = nombreMaquina;
	}

	public BigDecimal getOficinaIngresoWeb() {
		return this.oficinaIngresoWeb;
	}

	public void setOficinaIngresoWeb(BigDecimal oficinaIngresoWeb) {
		this.oficinaIngresoWeb = oficinaIngresoWeb;
	}

	public String getPermisoWeb() {
		return this.permisoWeb;
	}

	public void setPermisoWeb(String permisoWeb) {
		this.permisoWeb = permisoWeb;
	}

	public BigDecimal getTipo() {
		return this.tipo;
	}

	public void setTipo(BigDecimal tipo) {
		this.tipo = tipo;
	}

	public String getTipoUsuario() {
		return this.tipoUsuario;
	}

	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

}