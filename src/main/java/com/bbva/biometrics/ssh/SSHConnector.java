package com.bbva.biometrics.ssh;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

@Component
public class SSHConnector {

	/**
	 * Constante que representa un enter.
	 */
	private static final String ENTER_KEY = "\n";
	/**
	 * Sesión SSH establecida.
	 */
	private Session session;
	@Value("${ssh.ip_dir}")
	private String host;
	@Value("${ssh.user}")
	private String username;
	@Value("${ssh.password}")
	private String password;
	@Value("${ssh.port}")
	private int port;
	@Value("${ssh.pathRemote}")
	private String pathRemote;
	@Value("${ssh.pathLocal}")
	private String pathLocal;

	/**
	 * Establece una conexión SSH.
	 *
	 * @param username Nombre de usuario.
	 * @param password Contraseña.
	 * @param host     Host a conectar.
	 * @param port     Puerto del Host.
	 *
	 * @throws JSchException          Cualquier error al establecer conexión SSH.
	 * @throws IllegalAccessException Indica que ya existe una conexión SSH
	 *                                establecida.
	 */
	public boolean connect() throws JSchException, IllegalAccessException {
		boolean isConnected = false;
		if (this.session == null || !this.session.isConnected()) {
			JSch jsch = new JSch();
			this.session = jsch.getSession(username, host, port);
			this.session.setPassword(password);
			// Parametro para no validar key de conexion.
			this.session.setConfig("StrictHostKeyChecking", "no");
			this.session.connect();
			isConnected = true;
		} else {
			// throw new IllegalAccessException("Sesion SSH ya iniciada.");
			isConnected = this.session.isConnected();
		}
		return isConnected;
	}

	/**
	 * Ejecuta un comando SSH.
	 *
	 * @param command Comando SSH a ejecutar.
	 *
	 * @return
	 *
	 * @throws IllegalAccessException Excepción lanzada cuando no hay conexión
	 *                                establecida.
	 * @throws JSchException          Excepción lanzada por algún error en la
	 *                                ejecución del comando SSH.
	 * @throws IOException            Excepción al leer el texto arrojado luego de
	 *                                la ejecución del comando SSH.
	 */
	public final String executeCommand(String command) throws IllegalAccessException, JSchException, IOException {
		if (this.session != null && this.session.isConnected()) {
			// Abrimos un canal SSH. Es como abrir una consola.
			ChannelExec channelExec = (ChannelExec) this.session.openChannel("exec");
			InputStream in = channelExec.getInputStream();
			// Ejecutamos el comando.
			channelExec.setCommand(command);
			channelExec.connect();
			// Obtenemos el texto impreso en la consola.
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			StringBuilder builder = new StringBuilder();
			String linea;
			while ((linea = reader.readLine()) != null) {
				builder.append(linea);
				builder.append(ENTER_KEY);
			}
			// Cerramos el canal SSH.
			channelExec.disconnect();
			// Retornamos el texto impreso en la consola.
			return builder.toString();
		} else {
			throw new IllegalAccessException("No existe sesion SSH iniciada.");
		}
	}

	private String getDirectory() {
		try {
			String dir = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
			String dir2 = URLEncoder.encode(dir, "UTF-8");
			return dir2;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Cierra la sesión SSH.
	 */
	public final void disconnect() {
		this.session.disconnect();
	}

	/**
	 * Permite buscar el listado de archivos dependiendo de la opcion.
	 * 
	 * @throws IllegalAccessException
	 * @throws JSchException
	 * @throws SftpException
	 */
	public Boolean getFile(String pathRemoteFile) throws IllegalAccessException, JSchException, SftpException {
		if (this.session != null && this.session.isConnected()) {
			ChannelSftp channelSftp = (ChannelSftp) this.session.openChannel("sftp");
			channelSftp.connect();
			channelSftp.get(pathRemoteFile, pathLocal);
			// Se cierra todo
			channelSftp.exit();
			channelSftp.disconnect();
			return true;
		} else {
			throw new IllegalAccessException("No existe sesion SFTP iniciada.");
		}
	}

}
