package com.bbva.biometrics.interceptor;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpMethod;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.impl.TextCodec;
import javassist.NotFoundException;

public class JwtFilter implements Filter {
	private List<String> excludedUrls;
	private String secret;

	@Override
	public void init(final FilterConfig filterConfig) throws ServletException {
		String excludePattern = "/,/validate-device";
		this.excludedUrls = Arrays.asList(excludePattern.split(","));
	}

	@PostConstruct
	public void loadProp() throws IOException, NotFoundException {
		Properties prop = new Properties();
		InputStream input = null;
		String filename = "application.properties";
		input = getClass().getClassLoader().getResourceAsStream(filename);
		if (input == null) {
			throw new NotFoundException("file properties");
		}
		prop.load(input);
		secret = prop.getProperty("jwt.secret");
	}

	@Override
	public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
			throws IOException, ServletException {
		final HttpServletRequest request = (HttpServletRequest) req;
		final HttpServletResponse response = (HttpServletResponse) res;
		final String authHeader = request.getHeader("authorization");
		String path = request.getServletPath();
		if (path.contains(".html") || path.contains(".js") || path.contains(".css") || excludedUrls.contains(path)
				|| HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
			response.setStatus(HttpServletResponse.SC_OK);
			chain.doFilter(req, res);
		} else {
			if (authHeader == null || !authHeader.startsWith("Bearer ")) {
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			final String token = authHeader.substring(7);
			try {
				String tc = TextCodec.BASE64.encode(secret);
				final Claims claims = Jwts.parser().setSigningKey(tc).parseClaimsJws(token).getBody();
				request.setAttribute("claims", claims);
				request.setAttribute("user", claims.getSubject());
			} catch (final JwtException e) {
				e.printStackTrace();
				response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
				return;
			}
			chain.doFilter(req, res);
		}
	}

	@Override
	public void destroy() {

	}
}