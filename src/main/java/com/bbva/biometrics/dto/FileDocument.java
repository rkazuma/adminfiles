package com.bbva.biometrics.dto;

public class FileDocument {

	private String name;
	private String urlfile;
	private byte[] file;

	public FileDocument() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrlfile() {
		return urlfile;
	}

	public void setUrlfile(String urlfile) {
		this.urlfile = urlfile;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}

}
