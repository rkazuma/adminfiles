package com.bbva.biometrics.dto.security;

public class LoginRequest {
	private String ticket;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}	
}
