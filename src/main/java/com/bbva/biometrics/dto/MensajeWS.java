/**
 * MensajeWS.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bbva.biometrics.dto;

public class MensajeWS {
	private java.lang.String codigo;
	private java.lang.String mensaje;

	public MensajeWS() {
	}

	public MensajeWS(java.lang.String codigo, java.lang.String mensaje) {
		this.codigo = codigo;
		this.mensaje = mensaje;
	}

	/**
	 * Gets the codigo value for this MensajeWS.
	 * 
	 * @return codigo
	 */
	public java.lang.String getCodigo() {
		return codigo;
	}

	/**
	 * Sets the codigo value for this MensajeWS.
	 * 
	 * @param codigo
	 */
	public void setCodigo(java.lang.String codigo) {
		this.codigo = codigo;
	}

	/**
	 * Gets the mensaje value for this MensajeWS.
	 * 
	 * @return mensaje
	 */
	public java.lang.String getMensaje() {
		return mensaje;
	}

	/**
	 * Sets the mensaje value for this MensajeWS.
	 * 
	 * @param mensaje
	 */
	public void setMensaje(java.lang.String mensaje) {
		this.mensaje = mensaje;
	}

}
