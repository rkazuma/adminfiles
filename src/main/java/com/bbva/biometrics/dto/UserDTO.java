package com.bbva.biometrics.dto;

public class UserDTO {
	private String loginUsuario;
	private String claseDocumento;
	private Integer codigoGrupoUsuario;
	private String correo;
	private String descripcionUsuario;
	private String estadoUsuario;
	private String firmaUrl;
	private Integer identificacion;
	private String ip;
	private String nombreMaquina;
	private Integer oficinaIngresoWeb;
	private String permisoWeb;
	private Integer tipo;
	private String tipoUsuario;
	private String usuario;
	
	public UserDTO() {}
	public UserDTO(String loginUsuario, String descripcionUsuario, String estadoUsuario,
			String tipoUsuario) {
		this.loginUsuario = loginUsuario;
		this.descripcionUsuario = descripcionUsuario;
		this.estadoUsuario = estadoUsuario;
		this.tipoUsuario = tipoUsuario;
	}
	public String getLoginUsuario() {
		return loginUsuario;
	}
	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}
	public String getClaseDocumento() {
		return claseDocumento;
	}
	public void setClaseDocumento(String claseDocumento) {
		this.claseDocumento = claseDocumento;
	}
	public Integer getCodigoGrupoUsuario() {
		return codigoGrupoUsuario;
	}
	public void setCodigoGrupoUsuario(Integer codigoGrupoUsuario) {
		this.codigoGrupoUsuario = codigoGrupoUsuario;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getDescripcionUsuario() {
		return descripcionUsuario;
	}
	public void setDescripcionUsuario(String descripcionUsuario) {
		this.descripcionUsuario = descripcionUsuario;
	}
	public String getEstadoUsuario() {
		return estadoUsuario;
	}
	public void setEstadoUsuario(String estadoUsuario) {
		this.estadoUsuario = estadoUsuario;
	}
	public String getFirmaUrl() {
		return firmaUrl;
	}
	public void setFirmaUrl(String firmaUrl) {
		this.firmaUrl = firmaUrl;
	}
	public Integer getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(Integer identificacion) {
		this.identificacion = identificacion;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getNombreMaquina() {
		return nombreMaquina;
	}
	public void setNombreMaquina(String nombreMaquina) {
		this.nombreMaquina = nombreMaquina;
	}
	public Integer getOficinaIngresoWeb() {
		return oficinaIngresoWeb;
	}
	public void setOficinaIngresoWeb(Integer oficinaIngresoWeb) {
		this.oficinaIngresoWeb = oficinaIngresoWeb;
	}
	public String getPermisoWeb() {
		return permisoWeb;
	}
	public void setPermisoWeb(String permisoWeb) {
		this.permisoWeb = permisoWeb;
	}
	public Integer getTipo() {
		return tipo;
	}
	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	@Override
	public String toString() {
		return "UserDTO [loginUsuario=" + loginUsuario + ", claseDocumento=" + claseDocumento + ", codigoGrupoUsuario="
				+ codigoGrupoUsuario + ", correo=" + correo + ", descripcionUsuario=" + descripcionUsuario
				+ ", estadoUsuario=" + estadoUsuario + ", firmaUrl=" + firmaUrl + ", identificacion=" + identificacion
				+ ", ip=" + ip + ", nombreMaquina=" + nombreMaquina + ", oficinaIngresoWeb=" + oficinaIngresoWeb
				+ ", permisoWeb=" + permisoWeb + ", tipo=" + tipo + ", tipoUsuario=" + tipoUsuario + ", usuario="
				+ usuario + "]";
	}
	
}
