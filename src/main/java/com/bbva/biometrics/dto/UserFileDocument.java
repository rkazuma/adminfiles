package com.bbva.biometrics.dto;

import java.util.List;

public class UserFileDocument {

	private List<FileDocument> files;

	public List<FileDocument> getFiles() {
		return files;
	}

	public void setFiles(List<FileDocument> files) {
		this.files = files;
	}

}
